public class HideActivity extends Activity{

        private void hideIcon() {
		PackageManager p = getPackageManager();
		p.setComponentEnabledSetting(getComponentName(),
		PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
		PackageManager.DONT_KILL_APP);
	}
}